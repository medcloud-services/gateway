package app

import (
	"fmt"

	"gitlab.com/medcloud-services/gateway/app/config"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws"
	"go.uber.org/zap"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func Run() error {
	// Считываем настройки для этого сервера
	cnf := config.Get()

	// Инициализируем логгер
	zapLog.Init(cnf.Logger)
	defer func(Sug *zap.SugaredLogger) {
		err := Sug.Sync()
		if err != nil {
			_ = fmt.Errorf("gateway -> Sug.Sync() -> error: %s", err.Error())
		}
	}(zapLog.Sug)

	zapLog.Sug.Infof("gw -> Логер успешно инициализирован")

	// Запускаем ws концентратор клиентов.
	ws.RunHub()

	// Запуск http сервера
	if err := http.Start(); err != nil {
		return err
	}

	return nil
}
