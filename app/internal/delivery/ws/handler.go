package ws

import (
	"encoding/json"

	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws/clients"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

// msgData структура запроса к серверу от клиента
type msgData struct {
	FuncName string `json:"type"`
	Data     string `json:"data"`
}

// MsgResponse структура ответа от сервера клиенту
type MsgResponse struct {
	Status  int                    `json:"status"`
	Message string                 `json:"msg"`
	Salt    string                 `json:"salt"`
	Data    map[string]interface{} `json:"data"`
}

func requestHandler(c *clients.Client, msg []byte) {
	var data msgData

	var status int
	var res map[string]interface{}

	err := json.Unmarshal(msg, &data)
	if err == nil {
		zapLog.Sug.Info("ws -> requestHandler -> ", data)

		status, res = switchFunc(c, data)

	} else {
		zapLog.Sug.Warnf("ws handler -> prepareResponse -> %s", err)

		status = -1

		res = make(map[string]interface{})
		res["err"] = err.Error()
	}

	// подготовить ответ
	response := prepareResponse(status, res)

	byteResponse, errJson := json.Marshal(response)
	if errJson != nil {
		zapLog.Sug.Errorf("ws handler -> requestHandler -> Error converting to json for response. Причина: %s", errJson)
		return
	}

	// отправить ответ на запрос
	c.Send <- byteResponse
}

func prepareResponse(status int, res map[string]interface{}) (response MsgResponse) {
	switch status {
	case -1:
		response.Message = "Incorrect json in request: " + res["err"].(string)
		zapLog.Sug.Warnf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case 0:
		response.Message = "There is no data to answer."
		zapLog.Sug.Warnf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case 1:
		response.Message = "Success"
		response.Data = res
	case -2:
		response.Message = "Error parse data."
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case -20:
		response.Message = "Error database connection."
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case -21:
		response.Message = "Error database. Empty response."
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case -22:
		response.Message = "Error in database."
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case -23:
		response.Message = "Error getting query data from the database."
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case -3:
		response.Message = "Incorrect data json."
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO", response.Message)
	case -4:
		response.Message = "Incorrect token."
	case -5:
		response.Message = "Unauthorized user."

	default:
		response.Message = "Other Error"
		zapLog.Sug.Errorf("ws handler -> prepareResponse -> %s. INFO, %i", response.Message, status)
	}

	response.Status = status

	return
}
