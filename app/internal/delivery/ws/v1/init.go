package v1

import (
	"gitlab.com/medcloud-services/auth/app/pkg/jwt"
	"gitlab.com/medcloud-services/gateway/app/config"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws/clients"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

type UserInfo struct {
	Token string `json:"token"`
}

func InitUser(c *clients.Client, info UserInfo) (status int, res map[string]interface{}) {
	cnf := config.Get()

	data, err := jwt.GetTokenData(info.Token, cnf.Server.Token)
	if err != nil {
		zapLog.Sug.Warnf("InitUser -> jwt.GetTokenData err: %s", err.Error())
		status = -4
		return
	}

	c.ClientData.Id = data.Id
	c.ClientData.Fio = data.Fio
	c.ClientData.Mis = data.Mis
	c.ClientData.IsAuth = true

	status = 1
	return
}

func GetOnlineClients(c *clients.Client) (status int, res map[string]interface{}) {
	if !c.ClientData.IsAuth {
		status = -5
		return
	}

	cls := make(map[int]interface{})

	mis := c.ClientData.Mis

	// перебираем всех активных клиентов
	for v := range c.Hub.Clients {
		// Выбираем пользователей с одной мис
		if mis == v.ClientData.Mis {
			cls[v.ClientData.Id] = v.ClientData.Fio
		}
	}

	res = make(map[string]interface{})
	res["clients"] = cls

	status = 1
	return
}
