package ws

import (
	"net/http"

	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws/clients"
)

var hub = clients.NewHub()

func RunHub() {
	go hub.Run()
}

func GetHub() *clients.Hub {
	return hub
}

func NewClient(w http.ResponseWriter, r *http.Request) {
	clients.NewClient(hub, w, r, requestHandler)
}
