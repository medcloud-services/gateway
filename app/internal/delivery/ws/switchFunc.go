package ws

import (
	"encoding/json"

	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws/clients"
	v1 "gitlab.com/medcloud-services/gateway/app/internal/delivery/ws/v1"
)

func switchFunc(c *clients.Client, data msgData) (status int, res map[string]interface{}) {
	switch data.FuncName {
	case "initUser":
		var info v1.UserInfo

		errJson := json.Unmarshal([]byte(data.Data), &info)
		if errJson != nil {
			status = -2
			return
		}

		status, res = v1.InitUser(c, info)

	case "getOnlineClients":
		status, res = v1.GetOnlineClients(c)
	default:
		status = 0
	}

	return
}
