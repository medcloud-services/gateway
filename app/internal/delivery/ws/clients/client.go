package clients

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 5024
)

var newline = []byte{'\n'}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client является посредником между подключением websocket и концентратором (Hub).
type Client struct {
	Hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	Send chan []byte

	ClientData ClientData
}

// ClientData - информация о пользователе для хранения в ОЗУ сервера на время работы клиента с сервером.
type ClientData struct {
	Id     int
	Fio    string
	Mis    string
	IsAuth bool
}

func NewClient(hub *Hub, w http.ResponseWriter, r *http.Request, callBack func(*Client, []byte)) {
	// todo Быть осторожными с методом ниже!
	upgrader.CheckOrigin = func(r *http.Request) bool { return true } // NOTE: Разрешает крос запросы.

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		zapLog.Sug.Errorf("NewClient -> Error connect. Причина: %s", err)
		return
	}

	zapLog.Sug.Infof("NewClient -> Success connect")

	client := &Client{Hub: hub, conn: conn, Send: make(chan []byte, 256)}
	client.Hub.register <- client

	// Разрешить сбор памяти, на которую ссылается вызывающий объект, выполнив всю работу в новых goroutines.
	go client.writePump()
	go client.ReadPump(callBack)
}

// ReadPump Для каждого соединения запускается goroutine функция (ReadPump).
// Приложение гарантирует, что есть только readPump на соединение, выполняя считывание входящих сообщений из этой goroutine.
func (c *Client) ReadPump(callBack func(*Client, []byte)) {
	defer func() {
		c.Hub.Unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))

	c.conn.SetPongHandler(func(string) error {
		_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				zapLog.Sug.Errorf("ReadPump -> Error. Причина: %s", err)
			}
			break
		}

		callBack(c, message)
	}
}

// writePump перекидывает сообщения из концентратора в нужное websocket соединение.
//
// Для каждого соединения запускается goroutine функция (writePump).
// Приложение гарантирует, что есть только writePump на соединение, выполняя все записи из этой goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				zapLog.Sug.Infof("writePump -> Hub закрыл соединение.")
				_ = c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				zapLog.Sug.Infof("writePump -> Error start NextWriter. Причина: %s", err)
				return
			}

			zapLog.Sug.Infof("writePump -> Send: %s", BytesToString(message))
			_, _ = w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.Send)
			for i := 0; i < n; i++ {
				_, _ = w.Write(newline)
				_, _ = w.Write(<-c.Send)
			}

			if err := w.Close(); err != nil {
				zapLog.Sug.Errorf("writePump -> Error w.Close(). Причина: %s", err)
				// log.Println("Error send: ", err)
				return
			}
		case <-ticker.C:
			_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				// log.Println("Error send ticker: ", err)
				zapLog.Sug.Errorf("writePump -> Error send ticker. Причина: %s", err)
				return
			}
		}
	}
}

func BytesToString(data []byte) string {
	return string(data[:])
}
