package clients

// Hub maintains the set of active Clients and broadcasts messages to the
// Clients.
type Hub struct {
	// Registered Clients.
	Clients map[*Client]bool

	// Inbound messages from the Clients.
	broadcast chan []byte

	// register requests from the Clients.
	register chan *Client

	// Unregister requests from Clients.
	Unregister chan *Client
}

func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[*Client]bool),
	}
}

func (h *Hub) Run() {
	for {
		select {

		case client := <-h.register:
			h.Clients[client] = true

		case client := <-h.Unregister:
			// удаляем клиента и закрываем соединение
			if _, ok := h.Clients[client]; ok {
				delete(h.Clients, client)
				close(client.Send)
			}
			/*
				case message := <-h.broadcast:
					// делаем рассылку всем клиентам
					for client := range h.Clients {
						select {

						case client.send <- message:
							log.Printf("pre pre send " + client.clientID)
						default:
							log.Printf("pre send " + client.clientID)
							close(client.send)
							log.Printf("post send " + client.clientID)
							delete(h.Clients, client)
						}
				}*/
		}
	}
}
