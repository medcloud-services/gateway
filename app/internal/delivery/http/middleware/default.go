package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/medcloud-services/auth/app/pkg/jwt"
	"gitlab.com/medcloud-services/gateway/app/config"
)

func CheckToken(c *gin.Context) {
	cnf := config.Get()
	if !jwt.IsValid(c.GetHeader("WWW-Authenticate"), cnf.Server.Token) {
		c.JSON(401, map[string]interface{}{
			"msg": "GW: WWW-Authenticate does not contain a valid access key.",
		})
		c.Abort()
		return
	}
}

func CheckAppCode(c *gin.Context) {
	if c.GetHeader("App-Code") == "" {
		c.JSON(401, map[string]interface{}{
			"msg": "GW: App-Code cannot be empty.",
		})
		c.Abort()
		return
	}
}
