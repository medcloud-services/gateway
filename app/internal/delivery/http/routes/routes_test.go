package routes

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/v1/api"

	"github.com/spf13/viper"
)

// TODO тесты не корректны.
func TestMain(m *testing.M) {
	fmt.Printf("Start test\n")

	code := m.Run()

	fmt.Printf("Success WebSocket connect.\n")
	os.Exit(code)
}

func TestRequestGet(t *testing.T) {
	statusCode, _ := api.MakeRequest("", "GET", "http://"+viper.GetString("services.notificationManager.host")+"/api/v1/example/1", nil)
	if statusCode >= 400 {
		t.Error("Suspicious response status", statusCode)
	}

	fmt.Println("statusCode : ", statusCode)
}

func TestRequestPost(t *testing.T) {
	statusCode, _ := api.MakeRequest("", "POST", "http://"+viper.GetString("services.notificationManager.host")+"/api/v1/example/1", nil)
	if statusCode >= 400 {
		t.Error("Suspicious response status", statusCode)
	}

	fmt.Println("statusCode : ", statusCode)
}
