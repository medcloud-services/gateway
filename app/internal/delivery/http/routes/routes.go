package routes

import (
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/middleware"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/v1/api"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/v1/api/info"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/v1/api/response"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func InitRoutes() *gin.Engine {
	gin.SetMode(viper.GetString("server.mode"))

	r := gin.Default()

	store := cookie.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	// Обработчик WS соединения
	r.GET("/ws", func(c *gin.Context) {
		ws.NewClient(c.Writer, c.Request)
	})

	infoGroup := r.Group("/info")
	{
		infoGroup.Use(middleware.CheckToken)
		infoGroup.GET("check-user-online", info.CheckUserOnline)
	}

	notifyGroup := r.Group("/notification")
	{
		routeName := "notificationManager"

		notifyGroup.Use(middleware.CheckToken, middleware.CheckAppCode)
		notifyGroup.GET("*any", api.DefaultProxy(routeName))
		notifyGroup.PUT("*any", api.DefaultProxy(routeName))
		notifyGroup.POST("*any", api.DefaultProxy(routeName))
		notifyGroup.OPTIONS("*any", api.AddDefaultHeaders, response.Empty)
	}

	r.NoRoute(func(c *gin.Context) {
		msg := "Not found url: " + c.Request.RequestURI

		zapLog.Sug.Warnf("Routes -> NoRoute: " + msg)

		c.JSON(404, gin.H{"message": msg})
	})

	return r
}
