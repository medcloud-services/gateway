package http

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/medcloud-services/gateway/app/config"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/routes"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func Start() error {
	cnf := config.Get()

	gin.SetMode(cnf.Server.Mode)

	// Объявляем роуты
	rts := routes.InitRoutes()

	// Запуск сервера
	zapLog.Sug.Infof("gw -> http -> Start server")

	err := rts.RunTLS(viper.GetString("server.host")+":"+viper.GetString("server.port"), viper.GetString("server.certPath"), viper.GetString("server.keyPath"))
	if err != nil {
		if viper.GetString("server.mode") == "debug" {
			// Старт сервера без tsl. (Только для тестовой среды!)
			zapLog.Sug.Warnf("gw -> http -> Start server without TSL")
			err = rts.Run(viper.GetString("server.host") + ":" + viper.GetString("server.port"))
			return err
		} else {
			return err
		}
	}

	return nil
}
