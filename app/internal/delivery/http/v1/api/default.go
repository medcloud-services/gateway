package api

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func AddDefaultHeaders(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "Content-Type")
	c.Header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
}

func DefaultProxy(routeName string) gin.HandlerFunc {
	return func(c *gin.Context) {
		zapLog.Sug.Debug("GateWay -> ", routeName)

		AddDefaultHeaders(c)

		url := "http://" + viper.GetString("services."+routeName+".host") + "/api" + getProxyUrl(c.Request.RequestURI)

		statusCode, res := MakeRequest(c.GetHeader("App-Code"), c.Request.Method, url, c.Request.Body)
		if statusCode >= 400 {
			zapLog.Sug.Warnf("GateWay -> Suspicious response status: code=%d routeName=%s Body=%s Query=%s", statusCode, routeName, c.Request.Body, c.Request.URL.Query())
		}

		zapLog.Sug.Debugf("GateWay -> Response status: code=%d routeName=%s Body=%s Query=%s", statusCode, routeName, c.Request.Body, c.Request.URL.Query())
		c.JSON(statusCode, res)
	}
}

func MakeRequest(appCode, method, url string, body io.Reader) (int, map[string]interface{}) {
	client := http.Client{}
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return 400, map[string]interface{}{
			"Status": "Error",
			"msg":    "Request err: " + err.Error(),
		}
	}
	request.Header.Add("App-Code", appCode)

	resp, err := client.Do(request)
	if err != nil {
		return 503, map[string]interface{}{
			"Status": "Error",
			"msg":    "The requested service is temporarily unavailable",
		}
	}

	var result map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return 520, map[string]interface{}{
			"Status": "Error",
			"msg":    "The response could not be decoded",
		}
	}

	return resp.StatusCode, result
}

func getProxyUrl(requestUri string) string {
	words := strings.Split(requestUri, "/")

	var resUrl string
	for i, word := range words {
		if i > 1 {
			resUrl += "/" + word
		}
	}

	return resUrl
}
