package info

import (
	"encoding/json"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/http/v1/api/response"
	"gitlab.com/medcloud-services/gateway/app/internal/delivery/ws"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func CheckUserOnline(c *gin.Context) {
	hub := ws.GetHub()

	jsonData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		zapLog.Sug.Errorf("check-user-online, ioutil.ReadAll err: %s", err.Error())
		response.Error(c, "Ошибка получения данных. Причина: "+err.Error())
		return
	}

	var data struct {
		Ids []int `json:"ids"`
	}
	if err := json.Unmarshal(jsonData, &data); err != nil {
		zapLog.Sug.Errorf("check-user-online, json.Unmarshal err: %s", err.Error())
		response.Error(c, "Некорректные данные. Причина: "+err.Error())
		return
	}
	zapLog.Sug.Debug("check-user-online -> data: ", data)

	res := make(map[int]bool)
	for _, id := range data.Ids {
		res[id] = false
		for val := range hub.Clients {
			if id == val.ClientData.Id {
				res[id] = true
				break
			}
		}
	}

	zapLog.Sug.Debug("check-user-online -> res: ", res)

	response.Success(c, res)
}
