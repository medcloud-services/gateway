# GateWay

"Путевые врата" - единая точка доступа ко всем сервисам MedCloud. Отвечает за контроль авторизации и маршрутизацию запросов. (maybe: monitoring, cache)


Работа с приложением:

    make run - запуск в docker контейнере. (так же поднимается postgre) 
    make run_gw - запуск из IDE. (запуск в режиме поиска "гонки" данных)
    make build_gw - скомпилировать бинарный файл под linux. 
    make link - автоформатирование кода и поиск проблемных мест. 

!!! **ОБЯЗАТЕЛЬНО**: запуск "make link" перед коммитом.

P.S. более подробную инструкцию по работе с Go приложениями можно найти в Readme файле сервиса нотификации:
https://gitlab.com/medcloud-services/notification-manager

p.p.s. go env -w GOPRIVATE=gitlab.com/medcloud-services - нет уверен в необходимости. 