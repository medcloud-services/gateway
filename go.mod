module gitlab.com/medcloud-services/gateway

go 1.16

require (
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/spf13/viper v1.8.1
	gitlab.com/medcloud-services/auth v1.4.0
	gitlab.com/medcloud-services/support/zap-logger v1.1.0
	go.uber.org/zap v1.19.0
)
