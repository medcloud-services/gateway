run:
	docker-compose up  --remove-orphans --build

run_gw:
	go run -race cmd/gw/main.go

build_gw:
	env GOOS=linux go build gitlab.com/medcloud-services/gateway/cmd/gw

lint:
	gofumpt -w -s ./gateway/..
	golangci-lint run --fix

