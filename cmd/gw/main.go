package main

import (
	"log"

	"gitlab.com/medcloud-services/gateway/app"
)

func main() {
	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
